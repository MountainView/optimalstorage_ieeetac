\section{Problem Formulation}
We consider the problem of operating energy storage over a finite horizon $t \in \mathcal T$ to minimize a user's electricity bill, which includes an energy component and a peak demand component.

For every time period $t$, the user has a net demand $d_t$ which is the actual demand minus distributed generation such as roof-top solar. 
We assume that at the beginning of the horizon, by utilizing historical data, we have a certain demand forecast $\dh_t$ so that the actual demand is 
\begin{equation}
d_t = \dh_t + \epsilon_t, \quad t \in \mathcal T.
\end{equation}
Here the demand forecast should encapsulate the seasonality and many other deterministic features of the demand process, whereas the forecast error $\epsilon_t$ is assumed to be zero-mean and independently and identically distributed (i.i.d.), following some prescribed distribution with a probability density function $f(x)$ and the corresponding cumulative distribution function $F(x)$. Note that the distribution of forecast error $F$ is either supplied by a probabilistic forecasting procedure or fitted using historical forecast error data. Albeit the error distribution may itself be inaccurate, it has been proven a useful way to model the stochasticity of the system. As we will see later, a distinctive feature of our results is that they are immune to inaccuracies in the forecast error distribution $F$. 

We take a stylized energy storage model, for which the state of charge is denoted by $b_t$. The storage dynamics are
\begin{equation}\label{stordyn}
b_{t+1} = b_t + u_t, \quad t \in \mathcal T,
\end{equation}
where $u_t$ represents the charging and discharging operation with $u_t >0$ modeling charging and $u_t <0$ modeling discharging. 
We consider a slow time scale setup, where each discrete time slot represents an hour or longer such that the power limit of the storage is not constrained. The energy limit of the storage can then be modeled as
\begin{equation}
\Bu \le b_t \le \Bo, \quad t \in \mathcal T \cup \{T+1\},
\end{equation}
where $\Bu$ is the minimal allowed state of charge, and $\Bo$ is the energy capacity of storage.
Given the finite horizon formulation of the problem, it is sometimes desired to impose additional requirements on the terminal storage level (\eg, by requiring the end-of-month storage level to equal the initial storage level so that the problem can be periodically solved for each month). 
To this end, we require that 
\begin{equation}
\BTu \le b_{T+1} \le \BTo,
\end{equation}
where $\Bu \le \BTu \le \BTo \le \Bo$ are given parameters.

%Since $\Bu$ and $\Bo$ are the battery operation limits, the final constraints must be stiffer: $\BTu \ge \Bu$, and $\BTo \le \Bo$.

The total cost over the horizon is calculated as follows: 
For every time period $t$, the energy charge is calculated as $p (d_t+ u_t)$, where $p>0$ is the energy price specified by the tariff%\footnote{It can be the case that the energy price is time dependent as in the time-of-use tariffs. However, as the focus of this work is on the demand charge component, and the problem of storage control for time varying prices has been extensively investigated in the prior literature (cf. \cite{qin2012optimal}\cite{MITrampStor}), here we take the energy price to be constant. }. 
For the entire horizon, the peak demand charge is calculated as $q \cdot\max_{t\in \mathcal T} (d_t + u_t)$, where $q> 0$ is the rate for (peak) demand charge.  
Thus the ex-post cost function is 
\begin{equation}\label{obj}
C(\ub, \db) := \sum_{t\in \mathcal T} p (d_t + u_t) + q  \cdot \max_{t \in \mathcal T} (d_t + u_t).
\end{equation}

The decision process evolves as follows:
\begin{enumerate}
\item Before the first time period $t = 1$, the demand forecast, the distribution of forecast error, and the storage parameters are known. 
The storage starts at a preset level  $b_1 = B_1 \in [\Bu, \Bo]$.
\item At the beginning of period $t$, the demand of the previous time period $d_{t-1}$ is revealed. 
The storage operator needs to issue an instruction for storage control in period $t$, i.e., $u_t$.
\item During the period $t$, the state of charge is updated according to \eqref{stordyn}.
\item After time period $T$ (denoted as period $T+1$ and referred to as the terminal period), the total electricity bill is calculated according to \eqref{obj}.
\end{enumerate}
Let $z_t :=  \max_{1 \le \tau < t} (d_\tau + u_\tau)$ be the running maximum demand prior to time period $t$, with $z_1 := -\infty$. Given the battery dynamics, it is easy to observe that the system is Markov, with (continuous) state variable $(b_t, z_t)$, so that it suffices to optimize over the space of policies of the form $u_t = \pi_t(b_t, z_t)$\footnote{The policy is allowed to implicitly depend on static information that is available prior to the first time period such as demand forecast $\dh_t$, $t\in \Tcal$. }, $t \in \Tcal$ \cite{bertsekas2007dynamic}. 
%For convenience, we define the information set available at the beginning of time period $t$ as $Y_t$, $t \in \mathcal T$, such that $Y_t \subset Y_{t+1}$. 
Thus the stochastic control (SC) problem for storage operation takes the form of 

\begin{subequations}\label{opt:ST}
\begin{align}
J^\star_F := \min_{\pib \in \Pi} \quad & J_F(\pib) = \mathbb{E}_F \left[\sum_{t\in \mathcal T} p (d_t + u_t) + q z_{T+1} \right]\\
\mbox{s.t.} \quad  
& b_{t+1} = b_t + u_t,\quad\quad \quad\quad\quad t\in \mathcal{T},\\
& z_{t+1} = \max(z_t, d_t + u_t),\quad t\in \mathcal{T},\\
%& u_t \mbox{ is adapted to } Y_t\text{\footnotemark}, \quad\quad\quad\quad t\in \mathcal{T},\\
& u_t = \pi_t(b_t, z_t), \quad\quad\quad\quad\quad t\in \mathcal{T},\\
& \Bu\le b_{t} \le \Bo, \quad\quad\quad \quad\quad\quad t\in \mathcal{T},\\
& \BTu \le b_{T+1} \le \BTo,\label{opt:ST:c6}
\end{align}
\end{subequations}
where $\pib = (\pi_1, \dots, \pi_T)$, and $\Pi = \Pi_1\times \dots \times \Pi_T$ is the set of admissible control policies with $\Pi_t$ being the set of all functions that map the state $(b_t, z_t)$  to a feasible storage control action $u_t$. %and the goal is to identify the optimal control policy that maps the set of available information to the  storage control actions.
%Here 
%$z_{t+1}$ is the maximum net demand up to (and including) time period $t$.
%From the previous discussion, we have $\Bu \le B_1, \BTu, \BTo \le \Bo$.


%\footnotetext{In a measure-theoretic language, $\{Y_t: t\in \mathcal T\}$ is a filtration, and $u_t$ is measurable with respect to $Y_t$. In the case that $Y_t \backslash Y_{t-1}$ does not contains additional information from other sources (e.g. updated weather forecast for improving the forecast of solar generation and so the net demand), the $Y_t$ sequence is generated by the state sequence $(b_t,z_t)$, and one can treat $u_t$  simply as a function of $b_t$ and $z_t$ without loss of optimality. The majority of the subsequent discussion will be applied to any sequence of control actions induced by a sequence of such $u_t$ functions. As such, we also use $u_t$ and $\bm{u}$ to denote such a control action sequence, whenever there is no source of confusion. }