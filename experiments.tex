\section{Experimental Results}

The emerging trend of the integration of home level storage, such as the Tesla Powerwall, provides great potential for demand charge and demand reduction programs for residential customers.

We use a Pacific Gas and Electric (PG\&E) dataset to examine the performance of our proposed algorithm for peak demand reduction.
It contains anonymized and secure smart meter readings on electricity consumption for 1923 PG\&E residential customers for a period of one year spanning from 08/01/2010 to 07/31/2011 at 1 hour intervals.

The average hourly energy consumption per user is 28.0 kWh, and we assume the storage device we plan to use is a Tesla Powerwall battery pack with 6.4 kWh energy storage capacity. 

The current flat electricity rate for PG\&E customer is \$0.243/kWh. 
The current demand charge rate for medium- and large-scale users is \$17.84/kW for PG\&E~\cite{web:pge}, and \$16.20/kW for South California Edison(SCE)~\cite{web:sce}. We use \$17/kW as the projected peak demand charge in our experiments.

\begin{figure}[htbp]
    \begin{center}
    \includegraphics[width=0.5\textwidth]{../MatlabScripts/load.eps}
    \caption{Forecast and true load for single customer}
    \label{fig:load}
    \end{center}
\end{figure}

The Past-3-week averaging method is used for forecasting a single user's load a week ahead. 
Fig.~\ref{fig:load} shows the hourly forecast load and true load for one customer from 7/25/2011 to 7/31/2011.

\begin{figure}[htbp]
    \begin{center}
    \includegraphics[width=0.5\textwidth]{../MatlabScripts/forecast.eps}
    \caption{Net forecast load with battery control}
    \label{fig:forecast}
    \end{center}
\end{figure}

By implementing a conventional linear programming solver for the CE program~\ref{opt:CE} as well as the proposed optimal solver for the stochastic program~\ref{opt:ST}, the revised hourly net demand for both methods can be determined (see Fig.~\ref{fig:forecast}).
The forecast peak demand is decreased from 5.5 kW to 3.8 kW.
Moreover, it is clear that the proposed method not only reduces the highest peak but also reduces the sub-peaks and forms a bunch of ``plateaus.''

\begin{figure}[htbp]
    \begin{center}
    \includegraphics[width=0.5\textwidth]{../MatlabScripts/true.eps}
    \caption{Net true load with battery control}
    \label{fig:true}
    \end{center}
\end{figure}

The results of applying the two control action sequences to the true demand are shown in Fig.~\ref{fig:true}.
During the last week of July 2011, the customer's total energy charge is \$120.40, and the maximum demand is 5.47 kW at the 161st hour.
If the user had installed a Tesla Powerwall and followed the naive CE control actions, then the net maximum demand would have been 4.77kW at the 113rd hour.
If the user had deployed the proposed optimal control sequence for a stochastic program, the net demand maximum would have been reduced to 4.66kW, still at the 113rd hour of the week.
The difference in demand charge for the two battery control sequences is \$ 1.84 for a single peak for a single customer. 

\begin{figure}[htbp]
    \begin{center}
    \includegraphics[width=0.5\textwidth]{../MatlabScripts/diff.eps}
    \caption{Demand difference between the naive CE algorithm and the proposed optimal algorithm, versus the forecast error's standard deviation}
    \label{fig:diff}
    \end{center}
\end{figure}

The difference between the two methods' performances is merely due to the randomness of the forecast error.
We conducted a simulation of the relationship between a forecast error's standard deviation and the peak reduction difference for the two methods, and the results are shown in Fig.~\ref{fig:diff}.
The curve is simulated in a way that the battery control actions are based on true load; however, when we evaluate the performance of the actions, we add a series of Normal i.i.d. errors with zero mean and a level of standard deviation different from that of the true load.
In that case, the control sequences are applied to the modified load.
When there is no forecast load, the two methods' performance is the same. 
The proposed optimal method over-performs the simple CE method as long as the error's standard deviation increases, and the curve asymptontically becomes flat.
This shows that the advantage of the optimal control over offline control has some inherent limits, no matter how low the uncertainty of the forecast is.