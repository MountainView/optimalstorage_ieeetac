\section{Introduction}
%ref: Modeling and online control of generalized energy storage networks
%
%deep penetration of renewable sources.
%
%renewable sources are not predictable. Hard to validate i.i.d. assumptions.
%
%We want to flatten the spikes. One appealing answer is to use storage networks in future smart grids.
%
%List some papers on the topic to emphasize the research. mention that their results are heuristic.
%
%Describe our online control model.
%
%online flow model with a decay factor. This might be useful for other online models with decay factor, such as warehouse storage problems.


%%%%%%%%%%%%%%%


Renewables are expected to supply more than $50\%$ of electricity demand by $2050$  in various parts of the world \cite{CASB350, EU2050, Jacobson2015}. 
The intermittent nature of wind and solar energy challenges conventional \emph{reserve-based} operational schemes for the electric power grid as they are designed for small uncertainty scenarios \cite{Varaiya2011}.  
Given that forecast errors for wind and solar generation can be $5-10$ times larger than those for load \cite{Hong2014},  $50\%$ renewable penetration will result in substantially higher reserve requirements, which can be cost prohibitive and offset the environmental benefits of renewables as these reserve requirements are usually fulfilled by fast-ramping fossil-fueled generators.

Energy storage provides an alternative solution to maintain supply demand balance by transporting energy across time \cite{Dunn2011}.
Recent years have witnessed a strong positive trend for energy storage. 
On one hand, partly as a consequence of the increasing public and commercial interest in electric vehicles, the cost of energy storage is decreasing rapidly \cite{Nykvist2015}. 
On the other hand, many states have set policies to incentivize or mandate the adoption of energy storage. 
Examples include  California's 1.3 GW energy storage procurement mandate by $2020$ \cite{CA1.3GWStor} and New Jersey's Renewable Electric Storage Incentive Program \cite{NJRESIP}. 
This trend has led to significant industrial impacts, as evidenced by the record-breaking growth of US storage installations in $2015$ \cite{gtmR2015Q3Stor}. 

The question of optimal control of energy storage has been investigated with a variety of settings.
The problem of optimally arbitraging a stochastic price sequence using storage is analyzed using dynamic programming in \cite{QRsimpleStorPes2012}, where it is shown that the optimal control policy can be characterized by a sequence of price thresholds whose recursive expressions are  also obtained. 
Faghih \etal \cite{MITrampStor}  study the same problem with a more realistic storage model in which the power (ramping) limits of the storage devices are modeled. 
Problems of using energy storage to minimize energy imbalance and thus reduce the system risk are studied in various contexts: 
In \cite{SuEGTPS, RLDSACC} for reducing reserve energy requirements in power system dispatch, in \cite{BitarRACC_colocated, Powell} for operating storage co-located with a wind farm, in \cite{IBMload, DataCenter} for operating storage co-located with end-user demands, and in \cite{StorDRLongbo, Xu2014} for storage with demand response. 
Most of these studies obtain closed-form expressions or structural characterization of the optimal storage control policy by exploiting the specific form of the cost function considered. 
In a rather general setting, an online algorithm is constructed in \cite{Qin2015} for the problem of single storage control with any convex cost function and random demand,  where a bound is provided for a priori estimate of the algorithm's performance. 

Common to all these prior work is the assumption that the cost function is additively separable across time periods. 
Under this assumption, the problem of storage control is greatly simplified, as one can focus on the storage dynamics, which are often considered to be the only source of coupling of decision variables in different time periods.  
Although this assumption is accurate for many use cases of storage when the cost or benefit is accounted for period by period, this is not always the case, especially for behind-the-meter applications of storage.  
In particular, a nonseparable cost function is relevant when the tariff of the electricity utility company includes payment components depending 
on (monthly) aggregate statistics of electricity usage in addition to the total energy consumption. 

A preeminent example is that of \emph{demand charge}, which charges the customer a demand price $q>0$ multiplied by the \emph{maximum} or peak demand of the customer consumed in any hour of the month.  As the demand prices charged by utility companies can be as high as $100$ times of energy prices, demand charge could account for more than $50\%$ of the monthly electricity bill of a commercial customer.  
As a consequence, reducing the peak demand has been deemed the ``most economic use'' of energy storage when demand charge is in place\cite{Neubauer2015}. %and such service has been provided by a number of companies such as Stem, CODA Energy, and Green Charge Networks.
Furthermore, while the majority of smaller residential customers have never been subject to demand charge, recent trend of adoption of \emph{residential demand charge} by a number of utility companies indicates that the use of storage for reducing demand charge could have impact beyond the commercial electricity customers. 

We present here a study of the problem of stochastic control of energy storage for the purpose of jointly reducing the (volumetric) energy charge and demand charge. 
The stochastic demand in our model could be used to model the net demand, which is the actual demand minus the renewable generation from, for instance, rooftop solar generation. 
The paper contributes to the existing literature in the following ways: 
(i) We provide a stylized formulation of the problem, which reduces the challenge of time-coupling cost functions into a standard  stochastic control problem with an enlarged state space. 
(ii)  Using novel probabilistic arguments, we establish that this problem is \emph{certainty equivalent} (CE) whenever the deterministic counterpart of the problem has a unique solution. 
Our proof approach is significantly different from dynamic programming based arguments, which are widely used to establish common certainty equivalence results such as that for LQG. 
(iii) When the  deterministic counterpart of the problem has multiple solutions, some solutions can lead to better performance for the stochastic control program than others. 
In this case, we provide an algorithm constructing the best CE solution, which we also prove to be optimal for the stochastic counterpart.

\subsection{Organization}

The paper is organized as follows: 
Section II proposes the problem formulation.
Section III discusses the perfectly adequate storage case and proves the optimality of the CE problem solution.
Section IV extends the CE equivalency to arbitrary storage capacities as long as the solution of the CE problem is unique.
Section V provides an algorithm constructing the optimal solution for the stochastic problem and proves its optimality.
Experimental results are in section VI.
Section VII concludes the paper.

\subsection{Notation}
We use $t\in \mathcal T:= \{1,\dots, T\}$ to index time periods.  For any Euclidean vector space $\reals^d$, we use $\bold{1}\in \reals^d$ to denote the all-one vector.
For any real number $x\in \reals$,  we use $(x)^+:= \max(x,0)$ to denote the positive part of $x$ and $(x)^-:= -\min(x,0)$ to denote the negative part of $x$ so that $x = (x)^+ - (x)^-$. 

