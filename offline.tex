\section{Theoretical Part}

The original stochastic problem could be investigated using Bellman's recursion.
However, given the continuous state space at hand, exact evaluation of Bellman's recursion is very challenging.
In this paper, we first investigate the deterministic counterpart of the stochastic problem~\eqref{opt:ST}, and then use the properties of the deterministic counterpart's optimal solution to construct the optimal solution of the original stochastic problem.

\subsection{Peak Cycle of Deterministic Counterpart}
The deterministic counterpart~\eqref{opt:CE} optimizes storage charging and discharging based on the forecast demand, i.e.,
\begin{varsubequations}{D}
	\label{opt:CE}
	\begin{align}
	\Jh^\star := \min_{\bm{u}\in \mathbb{R}^T, \bm{z}\in \reals^{T+1}} \quad & \Jh(\bm{u}) = \sum_{t\in \mathcal T} p (\dh_t + u_t) + q z_{T+1} \\
	\mbox{s.t.} \quad &\bm{u} \in \Ucal, \label{opt:CE:u}\\
	& z_{t+1} = \max(z_t, \dh_t + u_t), \quad t\in \Tcal,
	\end{align}
\end{varsubequations}
where the set of all modified feasible control sequences is denoted by
\[
	\Ucal:= \left\{{\bm u} \in \reals^T: \Bu_{t+1} \le B_1 + \sum_{\tau=1}^t u_\tau \le \Bo_{t+1},\,\, t \in \Tcal \right\},
\]
with  $(\Bu_{t}, \Bo_{t}):= (\Bu, \Bo)$ for all $t\in \mathcal T$. 
Note that $(\Bu_{t}, \Bo_{t})$ is conveniently defined as the sequence of state of charge limits, which equals $ (\Bu, \Bo)$ for all periods except $t=T+1$. 
It is easily verified that~\eqref{opt:CE} can be written as a linear program, for which in general there exist multiple solutions.
The set of optimal solutions of the deterministic counterpart~\eqref{opt:CE} is denoted by $\Ucal^\star_\mathrm{D}$.

Under constant (volumetric) energy price $p$, \eqref{opt:CE} can be simplified by decoupling the energy cost, $\sum_{t\in \mathcal T} p (\dh_t + u_t)$, and the peak demand charge $qz_{T+1}$. 
In fact, we can restrict our attention to the set of storage control sequences that achieve the minimum energy cost by fully discharging (down to $\Bu_{T+1}$) the battery in the last time period, and we have:
\begin{lemma}\label{lemma:NonOptimal}
	For any feasible control sequence $\bm{u} \in \Ucal$ that achieves the optimal cost of \eqref{opt:CE}, i.e., $\Jh(\bm{u}) = \Jh^\star$, we have $\bm{u} \in \UR$, where
	\[
	\UR := \Ucal \cap \{\bm{u} \in \reals^T: \bm{1}^\top\bm{u} = \BTu - B_1 \}.
	\]
\end{lemma}
Therefore we can modify the constraint \eqref{opt:CE:u} to $u \in \UR$ without loss of optimality and focus only on the modified forecast demand peak:
\[
	\zh({\bm u}) := \max_{t\in \mathcal T} \dh_t + u_t,
\]
because the total energy charge of all control sequences $u \in \UR$ is a constant, $p\left(\BTu - B_1 + \sum_{t\in \mathcal{T}} \dh_t\right)$.
We also denote the optimal modified forecast demand peak, $\zh({\bm u}^\star)$ for all ${\bm u}^\star \in \Ucal^\star_\mathrm{D}$ as $\zh^\star$.

As intuitively expected and rigorously established later, of central importance to both the deterministic and stochastic version of this problem is the set of time periods in which the modified demand process peaks.
This motivates the following definition:
\begin{definition}[Peak periods and peak cycles]
	Given an arbitrary control sequence $\bm u$ and the corresponding modified forecast demand $\bm \dh + \bm u$,  the set of time periods in which the modified forecast demand achieves its maximum is referred to as \emph{peak periods} of the modified demand and is defined as the ordered set
	$$
		\TP_{\bm u} :=\left \{t \in  \mathcal T: u_t + \dh_t = \zh({\bm u})\right\}.
	$$
	When $\TP_{\bm u}$ contains multiple disjoint segments of consecutive time periods, let any of its subsets of the form
	\[
		\CP =  \{\ttu, \ttu+1, \ldots, \tto\},~\ttu \leq \tto,~\ttu,\tto \in \TP_{\bm u},
	\]
	be deemed a \emph{peak cycle} if $\CP$ is a maximal consecutive subsequence of $\TP_{\bm u}$, i.e., if  any consecutive subset $\widetilde{\mathcal C} \subseteq \TP_{\bm u}$ satisfies either $\widetilde{\mathcal C}  \subseteq \CP$ or $\widetilde{\mathcal C}  \cap \CP = \emptyset$. We also denote the collection of all peak cycles by $ \mathbb{C}_{\bm u}$.
\end{definition}

% For example, if $T =24$, and for a given $\bm \dh$, a possible  $\TP_{\bm u} $ could be $\{2,3,4, 10,11,23\}$ in which  $\{2,3,4\}$, $\{10,11\}$, $\{23\}$ are 3 peak cycles.

\begin{figure}[htbp]
    \begin{center}
    \includegraphics[width=0.5\textwidth]{Figures/ce.png}
    \caption{Illustration of one optimal solution of the certainty equivalent problem}
    \label{fig:ce}
    \end{center}
\end{figure} 

Fig.~\ref{fig:ce} visualizes peak cycles and peak periods for one optimal solution of the deterministic counterpart~\eqref{opt:CE}.
The gray curve represents the original forecast load.
The blue dashed curve represents the modified forecasted load, where the peak periods are highlighted by blue shade.
For this modified demand, the set of peak periods consists of only one peak cycle.

As we will show, depending on the storage charging/discharging profile, certain peak cycles are more important than others.
We first introduce the definition of \emph{critical sub peak cycles}:

\begin{definition}[Critical sub peak cycles] 
\label{def:pcs}
For a given set of peak periods $\TP_{\bm u}$, let its subsets of the form 
	\[
	\CPS =  \{\ttu', \ttu'+1, \ldots, \tto'\},~\ttu' \leq \tto',~\ttu', \tto' \in \TP_{\bm u},
	\]
	be deemed a \emph{critical sub peak cycle} if $b_{\tto'+1} = \Bu_{\tto'+1}$ and $b_{\ttu'} = \Bo_{\ttu'}$.
% $\ttu'$ and $\tto'$ are denoted by the critical boundary of the critical sub peak cycle.

\end{definition}

By definition, any critical sub peak cycle is a subset of some peak cycle. 
Notice that not all peak cycles have a critical sub peak cycle.
In Fig.~\ref{fig:ce}, the sub peak cycle is simply the only peak cycle itself.

In other words, a critical sub peak cycle if it exists is associated with some control sequence $\bm{u}$, and is a consecutive time period during which (i) the control sequence $\bm u$ would fully charge the battery before the first period, (ii) the control sequence $\bm u$ has fully discharged the battery, and (iii) the modified forecasted demands stay at the maximum.

Intuitively, for any peak cycle containing critical sub peak cycles, it is impossible to further reduce the maximal demand of the peak cycle.
However, for any peak cycle $\CP =  \{\ttu, \ldots, \tto\}$ that does not contain any critical sub peak cycles, we can always reduce the maximal of the peak cycle, by adjusting the charging/discharging $u_t$, for time periods $t \in \CP \cup \{t_\mathrm{s-1}, t_\mathrm{e+1}\}$.

In fact, we can show that it is possible to certify the optimality of a control sequence $\bm u$ for the deterministic counterpart~\eqref{opt:CE} by checking only the existence of critical sub peak cycles:

\begin{lemma}\label{lemma:SuOpt}
For all $\bm{u} \in \UR$, $\zh(\bm{u}) = \zh^\star$ if and only if there exists at least one critical sub peak cycle.
%, where $(\Bu_{t}, \Bo_t)$, $t\in \mathcal T \cup \{T+1\}$, is the sequence of battery state limits as defined in Section~\ref{sec:flat}. 
\end{lemma}
\opt{opta} {
\input{Proofs/Suopt}
}
\opt{optb} {
	A proof of Lemma~\ref{lemma:SuOpt} is in Appendix.
}