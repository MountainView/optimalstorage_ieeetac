\section{Optimal Certainty Equivalent Control}\label{sec:oce}

When the storage capacity is limited, $\Ucal^\star_\mathrm{CE}$ often contains multiple solutions.
% Arbitrary CE control v.s. Optimal CE control
% \captionsetup[subfloat]{farskip=-30pt, position=bottom}
\begin{figure}[htbp]
\subfloat[Forecasted load]{\label{fig:cest}\includegraphics[width=0.45\textwidth]{Figures/cest.png}
}\\
\subfloat[Actual load]{\label{fig:cestr}\includegraphics[width=0.45\textwidth]{Figures/cestr.png}
}\\
\subfloat[Battery status]{\label{fig:cestb}\includegraphics[width=0.45\textwidth]{Figures/cestb.png}
}
  \caption{Illustration of CE optimal solution and stochastic optimal solution}
  \label{fig:cests}
\end{figure}

%Figure~\eqref{fig:nonunique_example} illustrates an example for which $\Ucal^\star_\mathrm{CE}$ has more than one solution. 
Unlike in the case of the usual certainty equivalent situations, where any CE solution would be optimal, here, as shown in Figure~\ref{fig:forecast}, some CE solutions may have strictly lower cost for the stochastic program~\eqref{opt:ST}.  
Thus it is important to identify the CE solution that is optimal for the stochastic program~\eqref{opt:ST}. In the following, we provide an explicit construction. 

%\subsection{Necessary Condition for Optimality of CE Program Control Sequence}

%Subsection~\ref{subsec:SuOpt} 

We start by providing an explicit characterization of the set of optimal controls 
 for the CE program~\eqref{opt:CE}.

%Actually, the sufficient condition for CE program optimality is also necessary: for all CE program optimal solutions, the condition in \eqref{lemma:SuOpt} must hold, and furthermore, all optimal solutions share the same such consecutive sequences:

\begin{theorem}
\label{thm:NeOpt}

If $\CPS$ is a critical sub peak cycle for one $\bm{u} \in \mathcal{U}_{\mathrm{CE}}^\star$, then it is also a critical sub peak cycle for all $\bm{u} \in \mathcal{U}_{\mathrm{CE}}^\star$.

\end{theorem}

\input{Proofs/NeOpt}

Specifically, from Theorem~\ref{thm:subset}, any critical sub peak cycle is also shared by any optimal control sequence for a stochastic program~\eqref{opt:ST} in $\mathcal U^\star$. 
If we can find a critical sub peak cycle of $\UCE$, then we can, with the information of the optimal maximum demand $\zh^\star$, determine optimal control actions at the shared peak period by simply deducting the forecast load from $\zh^\star$.

Corollary~\ref{cor:CEtoSE} provides a practical method to find a shared peak period of $\UCE$ by picking an arbitrary solution in $\UCE$:

\begin{corollary}\label{cor:CEtoSE}
For any $\bm{\uh}^\star \in \UCE$, if for some $\ttu$, $\tto$ such that
$
\uh_{t}^\star + \dh_{t}  =\zh^\star,
$
for all $t = \ttu, \ldots, \tto$, and 
$
\sum_{t=\ttu}^{\tto} \uh_t^\star = \Bu_{\tto+1} - \Bo_{\ttu},
$ then $\bm{u}^\star \in \Ucal^\star$, where $\Ucal^\star$ is the set of optimal control actions for stochastic program~\eqref{opt:ST}.
\end{corollary}

%\subsection{Find common control actions reaching maximum net demand}

This allows us to access $\bm u^\star$ at least for the time periods inside of the peak cycle, provided that (i) we can find the peak cycle, and (ii) we know the peak for CE. 

Corollary~\ref{cor:CEtoSE} provides a very practical method to determine the optimal stochastic control actions at some time stamps by solving a certainty equivalent problem and finding the consecutive time-stamp sequence where all optimal control actions for the CE program~\eqref{opt:CE} have the same charging/discharging control, and the new net demands equal the maximum net demand at these time stamps.

\begin{algorithm}[htbp]
\caption{PeakFinder}\label{alg:PF}
\begin{algorithmic}[1]
\Procedure{PeakFinder}{$\bm{\dh}, \bm{\beta}$}
\State $T\gets$ Length of $\bm{\dh}$
\State $u_1^\star, \ldots, u_T^\star \gets \Call{CESolver}{\bm{\dh}, \bm{\beta}}$\label{ce:solver}
\State $b_1^\star \gets \beta_3$
\State $b_i \gets b_{i-1} + u_{i-1}^\star$, $i = 2, \ldots, T+1$

\State $z^\star \gets \max_{i=1, \ldots, T}\left(u_i^\star + \dh_i\right)$
\State $U \gets \emptyset$; $\ttu \gets -1$; $\tto \gets T$
\For {$i \gets 1, \ldots, T $}
	\If {$b_i^\star = \beta_1$ and $U \neq \emptyset$}
	\State $\tto \gets i-1$; \textbf{Break}
	\EndIf
	\If{$u_i^\star + \dh_i \neq z^\star$}
		\State $U \gets \emptyset$; $\ttu \gets -1$
	\ElsIf{$U \neq \emptyset$ or $b_i^\star=\beta_2$ or $i = 1$}
		\If{$\ttu = -1$}
		\State $\ttu \gets i$
		\EndIf
		\State $U \gets U \cup \{u_i^\star\}$ 
	\EndIf
\EndFor
\State \textbf{Return} $U$, $\ttu$, $\tto$
\EndProcedure
\end{algorithmic}
\end{algorithm}

The pseudocode of the algorithm, \verb+PEAKFINDER+, is introduced in Algorithm~\ref{alg:PF}.

The inputs are $\bm{\dh}$, the forecasted load at time stamp $1, \ldots, T$, and a vector $\bm{\beta} = [\beta_1, \beta_2, \beta_3, \beta_4]$ indicating the storage capacity constraints: $\beta_1 = \Bu$ is the storage state lower bound, $\beta_2 = \Bo$ is the storage state upper bound, $\beta_3 = B_1$ is the initial storage state and $\beta_4 = B_{T+1}$ is the terminal storage state.

The CE program~\eqref{opt:CE},  a simple linear programming problem, could be efficiently solved by many standardized methods, which are denoted as \verb+CESOLVER+ in line 3.
After obtaining one optimal solution from \verb+CESOLVER+, 
\verb+PEAKFINDER+ iterates through all time stamps to find the starting and stopping position, $\ttu$ and $\tto$, of a time period,
% as well as the corresponding optimal control action set $U$, where 
during which the new net demand reaches maximal net demand, and the battery is fully charged at the beginning of the sequence and is fully discharged at the end of the sequence.
Theorem~\ref{thm:NeOpt} guarantees that $U \neq \emptyset$.

\subsection{Decoupling of Optimal Control Sequence}

The peak cycle, $\ttu, \ldots, \tto$, partitions the control time period $\Tcal$ to at most 3 sub-periods:
$$
\Tcal_L := \{1, \ldots, \ttu-1\},
$$
$$
\Tcal_P := \{\ttu, \ldots, \tto\},
$$
$$
\Tcal_R := \{\tto+1, \ldots, T\}
$$
where $\Tcal_L$ does not exist when $\ttu = 1$ and $\Tcal_R$ does not exist when $\tto = T$.
And for any vector $\bm{a} \in \mathbb{R}^T$, we denote $\bm{a}_L$, $\bm{a}_P$, $\bm{a}_R$ are the projection of $\bm{a}$ on $\Tcal_L$, $\Tcal_P$, $\Tcal_R$, respectively.

Indeed, the peak cycle ``partitions'' the set of optimal control sequences for the CE program~\eqref{opt:CE} as follows:

given a peak cycle, $\ttu, \ldots, \tto$ for CE program~\eqref{opt:CE}, all $\bm{\uh}^\star \in \UCE$ must satisfy
\begin{align*}
\bm{1}^T \bm{\uh}_L^\star &= \Bo_{\ttu} - B_1,\\
 \bm{1}^T \bm{\uh}_P^\star &= \Bu_{\tto+1} - \Bo_{\ttu},\\
\bm{1}^T \bm{\uh}_R^\star &= \Bu_{T+1} - \Bu_{\tto+1},\\
\bm{\uh}_P^\star &= \zh^\star - \bm{\dh}_P.
\end{align*}

\subsection{Recursive Method to Determine All Optimal Control Actions for Stochastic Program}

We could obtain the full solution by solving the two sub-problems below:

\begin{subequations}\label{opt:CEL}
\begin{align}
\zh_L^\star := \min \quad & \zh_L(\bm{u}) = z_{\ttu} \\
\mbox{s.t.} \quad & \bm{u} = [u_1, \ldots, u_{\ttu-1}]\\
&\bm{1}^T \bm{u} = \Bo_{\ttu} - B_1,\\
& \Bu_t \le B_1 + \sum_{\tau=1}^t u_\tau \le \Bo_t, \quad t\in \Tcal_L\\
& z_{t+1} = \max(z_t, d_t + u_t), \quad t\in \Tcal_L,
\end{align}
\end{subequations}

and 

\begin{subequations}\label{opt:CER}
\begin{align}
\zh_R^\star := \min \quad & \zh_R(\bm{u}) = z_{T+1} \\
\mbox{s.t.} \quad & \bm{u} = [u_{\tto +1}, \ldots, u_{T+1}] \\
&\bm{1}^T \bm{u} = \Bu_{T+1} - \Bu_{\tto+1},\\
& \Bu_t \le \Bu_{\tto+1} + \sum_{\tau=\tto+1}^{t} u_\tau \le \Bo_t, \quad t\in \Tcal_R\\
& z_{t+1} = \max(z_t, d_t + u_t), \quad t\in \Tcal_R.
\end{align}
\end{subequations}

Notice that, the CE programs~\eqref{opt:CEL} and~\eqref{opt:CER} have exactly the same structure as the CE program~\eqref{opt:CE}, with a reduced length of time stamps.

Moreover, for all $\bm{u} \in \UCE$,  $\bm{u}_L$ and $\bm{u}_R$ are feasible solutions for \eqref{opt:CEL} and~\eqref{opt:CER}, respectively, and we have:

\begin{lemma}\label{lemma:Recursion}
For all $\bm{u} \in \Ucal^\star$, which is the optimal control sequence for stochastic program~\eqref{opt:ST},
$$
\bm{u}_L \in {\UCE}_L, \text{~and~} \bm{u}_R \in {\UCE}_R,
$$
where ${\UCE}_L$, ${\UCE}_R$ are the sets of optimal control sequences for~\eqref{opt:CEL} and~\eqref{opt:CER}, respectively.
\end{lemma}

\input{Proofs/Recursion}

Accordingly, after the partitioning by the peak cycle, $\ttu, \ldots, \tto$, we can determine the optimal control sequence for the stochastic problem~\eqref{opt:ST} in the peak cycle and divide the time period into 3 shorter time periods.
Furthermore, by solving the CE programs~\eqref{opt:CEL} and ~\eqref{opt:CER}, we can find the peak cycles for ~\eqref{opt:CEL} and ~\eqref{opt:CER} and determine the optimal control sequence for the stochastic problem~\eqref{opt:ST} on the peak cycles of ~\eqref{opt:CEL} and ~\eqref{opt:CER}.

In summary, by iteratively solving a series of CE programs and finding their associated peak cycles, we can determine the optimal control sequence for the stochastic problem~\eqref{opt:ST} at all time stamps, which is illustrated in Algorithm~\ref{alg:ST}.

\begin{algorithm}[htbp]
\caption{STSolver}\label{alg:ST}
\begin{algorithmic}[1]
\Procedure{STSolver}{$\bm{\dh}, \bm{\beta}$}
\State $T\gets$ Length of $\bm{\dh}$
\State $U, \ttu, \tto \gets \Call{PeakFinder}{\bm{\dh}, \bm{\beta}}$
\State $u_{\ttu}^\star, \ldots, u_{\tto}^\star \gets U$
\If{$\ttu > 1 $}
	\State $\bm{\dh}_L \gets [\dh_1, \ldots, \dh_{\ttu-1}]$
	\State $\bm{\beta}_{L} \gets [\beta_1, \beta_2, \beta_3, \beta_2]$
	\State $u_1^\star, \ldots, u_{\ttu-1}^\star \gets \Call{STSolver}{\bm{\dh}_L, \bm{\beta}_{L}}$
\EndIf

\If{$\tto < T $}
	\State $\bm{\dh}_R \gets [\dh_{\tto+1}, \ldots, \dh_{T}]$
	\State $\bm{\beta}_{R} \gets [\beta_1, \beta_2, \beta_1, \beta_4]$
	\State $u_{\tto+1}^\star, \ldots, u_{T}^\star \gets \Call{STSolver}{\bm{\dh}_R, \bm{\beta}_{R}}$
\EndIf
 \label{EstimateMean}

\State \textbf{return} $u_1^\star, \ldots, u_T^\star$

\EndProcedure
\end{algorithmic}
\end{algorithm}

In Algorithm~\ref{alg:ST}, the format of the input is the same as that in Algorithm~\ref{alg:PF}.
Line 3 calls \verb+PEAKFINDER+ to find one peak cycle and the associated optimal control actions.
Then a partition of forecast demand $\dh$ is conducted, and the associated new initial state and terminal state of storage of each partitioned battery control action sequence are updated.
The optimal control actions on $\Tcal_L$ and $\Tcal_R$ are recursively solved.

When the length of $\bm{\dh}$ is 1, there is no degree of freedom for battery charging/discharging if the initial and terminal states are given. In this case, \verb+PEAKFINDER+ simply returns the only available control action.
Another exit mechanism for \verb+PEAKFINDER+'s recursion is that the peak cycle is the whole period, which means storage is perfectly adequate.

At each level of recursion, the existence of the peak cycle guarantees that we can determine at least one optimal control action, and the maximum problem size at the next level's recursion is $n-1$, where $n$ is the problem size at the current recursion level.
Hence, the maximum recursion level is the length of the original control time period, $T$.

According to the discussion above, we have:
\begin{theorem}
\verb+STSOLVER+ in Algorithm~\ref{alg:ST} provides the optimal solution for stochastic program~\eqref{opt:ST}.
\end{theorem}